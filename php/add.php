<?php
$start = microtime(true);
$x = $_POST['x'];
$y = $_POST['y'];
$r = $_POST['r'];

$validate = true;
$xVal = array(-4, -3, -2, -1, 0, 1, 2, 3, 4);
$rVal = array(1, 2, 3, 4, 5);

if ($x == "-0") $x = 0;
if ($y == "-0") $y = 0;
if ($r == "-0") $r = 0;

if (!in_array($x, $xVal)) $validate = false;
elseif ($y<-3 || $y>5 || !is_numeric($y)) $validate = false;
elseif (!in_array($r, $rVal)) $validate = false;

if ($validate == false) die('validate error');

$dbObject = '';
$dbObject .= $x . ', ';
$dbObject .= $y . ', ';
$dbObject .= $r . ';';

// I    - Полуокружность с радиусом R
// II   - Прямоугольник R * R/2
// III  - y = -0.5x - 0.5R
// IV   - пустое множество
if ($x >= 0 && $y >= 0 && $x * $x + $y * $y <= $r * $r) $dbObject .= "Попал;";
elseif ($x <= 0 && $y >= 0 && $x >=  $r * -0.5 && $y <= $r) $dbObject .= "Попал;";
elseif ($x <= 0 && $y <= 0 && $y >= -0.5 * $x - 0.5 * $r ) $dbObject .= "Попал;";
else $dbObject .= "Не попал;";

$time = number_format((microtime(true)-$start)* 1000000, 0);

$dt = new DateTime("now", new DateTimeZone('Europe/Moscow'));
$dbObject .= $dt->format('Y-m-d H:i:s') . '[Moscow];';
$dbObject .= $time;
$dbObject.= "\n\r";

file_put_contents('db.csv', $dbObject, FILE_APPEND);
echo $dbObject;
?>
