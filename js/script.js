var button;
var y;
var x= -10;
var xcheck;
var r = -10;
var rcheck;
var img = new Image;
var err;
img.src = "https://se.ifmo.ru/~s335055/web/png/graph.png";
var context;

var aa = "1"
var bb = "1"
console.log(aa + bb)
console.log(aa - bb)
console.info((5 - 5 % 3)/ 3)

function start(){
    button = document.getElementById("button");
    y = document.getElementById("y");
    xcheck = document.getElementById("x");
    rcheck = document.getElementById("r");
    y.addEventListener("click", clear);
    xcheck.addEventListener("click", clear);
    rcheck.addEventListener("click", clear);
    button.addEventListener("click", validation);
    getTable();
}



function clearRButtons(){
    var Buttons= document.getElementById("r").getElementsByTagName("input");
    var Button=0;
    while(Button<Buttons.length){
        if(Buttons[Button].getAttribute("type")=="button"){
            Buttons[Button].className="NotActive";
        }
        Button++;
    };
}

function clearXButtons(){
    var Buttons= document.getElementById("x").getElementsByTagName("input");
    var Button=0;
    while(Button<Buttons.length){
        if(Buttons[Button].getAttribute("type")=="button"){
            Buttons[Button].className="NotActive";
        }
        Button++;
    };
}

function setR(newR, element) {
    clearRButtons();
    element.className = "Active";
    r = newR;
}

function setX(newX, element) {
    clearXButtons();
    element.className = "Active";
    x = newX;
}

function validation(){
    err = document.getElementById("error1");
    if (y.value.valueOf() > -3 && y.value.valueOf() < 5 && y.value .valueOf()!= "" && x != -10 && r != -10){
        makeDote(x, y.value.valueOf(), r);
        send(x, y.value.valueOf() ,r);
    } else {
        err.style.opacity = "1";
        err.style.display = "inline";
    }
}

function clear(){
    err = document.getElementById("error1");
    err.style.opacity = "0";
}

function graph(){
    window.onload = function(){
        var graphycs = document.getElementById("graph");
        if(graphycs && graphycs.getContext) {
            context = graphycs.getContext('2d');
            context.drawImage(img,0,0, 330, 330);
        }
    }
}

function makeDote(x, y, r){
    xcord = x/r * 120 + 165;
    ycord = 330 -(y/r * 120 + 165);
    if ( xcord > 330 || ycord > 330){
        return;
    }
    context.clearRect(0,0,330,330)
    context.drawImage(img,0,0,330,330);
    context.fillRect(xcord,ycord,3,3);
    context.fill();
}

function send(x, y, r){
    const xhr= new XMLHttpRequest();
    var data = new FormData();
    data.append('x', x);
    data.append('y', y);
    data.append('r', r);
    const url = "https://se.ifmo.ru/~s335055/web/php/add.php";
    xhr.open("POST", url, true);

    xhr.addEventListener("load", () => {
        if (xhr.status === 200) {
            console.log("script completed");
	        getTable();
        } else {
            alert( xhr.status + ': ' + xhr.statusText );
        }
    });

    xhr.send(data);
    
}

function getTable() {
    var serverResponse = document.querySelector('#response');
    const xhr = new XMLHttpRequest();
    const url = "https://se.ifmo.ru/~s335055/web/php/tableBuilder.php"

    xhr.open('POST', url, true);
    err = document.getElementById("error2");
    xhr.addEventListener("load", () => {
        console.info(xhr.responseText);
        if (xhr.status === 200) {
            serverResponse.innerHTML = xhr.responseText;
            err.style.opacity = "0";
        } else {
            err.style.opacity = "1";
            err.style.display = "inline";
        }
    });
    xhr.send();
}

graph();